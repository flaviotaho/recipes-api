# Recipes API

This project aims to create a REST API to manage Recipes using MongoDB and Python without a micro framework.
It is organized using a Functional Programming paradigm and its best practices.

## Installation

To run this project, you can run with docker's *docker-compose* or you can run it locally with Python. If you choose to run directly from your environment, it's necessary to have the following tools installed:

- python 3.10
- MongoDB
- virtualenv

## How to start

    $ git clone https://gitlab.com/flaviotaho/recipes-api.git
    $ cd recipes-api
    $ docker-compose up

## Running tests

This project is set to use `pytest`. To run it, you can run the following example:


    $ cd recipes-api
    $ pip install -r requirements.txt
    $ pytest -v



## Database

This API connects to a Mongo Database to save data into the following collections:
```
- recipes
    - _id: ObjectID         | Unique ID
    - name: str             | Name
    - prep_time: float      | Prep time
    - difficulty: int       | Difficulty (1-3)
    - vegetarian: bool      | Vegetarian (boolean) 

- recipes_rating
    - _id: ObjectID         | Unique ID
    - recipe_id: ObjectID   | Recipe's Unique ID
    - user_id: ObjectID     | User's ID

- users
    - _id: ObjectID         | User's ID
    - user_id: str          | User's Login
    - password: str         | User's Encrypted password
```

## Endpoints

For now, we have two main endpoint groups.
```
- recipe:
    - GET /recipes 
    - POST /recipes 
    - PUT /recipes/<recipe_id> 
    - GET /recipes/<recipe_id> 
    - DELETE /recipes/<recipe_id>
    - POST /recipes/<recipe_id>/rating 

- auth:
    - POST /signup
        this endpoint expects an object with user_id and password
    - POST /authenticate
        this endpoint authenticate by requiring a user_id and a password and retrieving a JSON Web Token
```

## Environment Variables

```
HOST: # ip address for this API to run
PORT: # port bound
MONGODB_CONNECTION_URI: # database connection with the following format `mongodb://user:password@host:port`
MONGODB_DATABASE_NAME: # recipes database name
FERNET_KEY: # fernet key for user's password cryptography
AUTH_SECRET_KEY: # JSON Web Token secret key
```

## Recipe Search Endpoint

This endpoint is ready to receive recipe properties for documents filtering. It can be search by **name**, **vegetarian**, **difficulty** and **preparation time**.
These parameters are received by query string, due to the possibility of HTTP caching strategies.
For the field's difficulty and preparation time, the query string parameters are split with two branches, min, and max, e.g. difficulty_max, prep_time_min.

## Next Steps

For the next improvements for this API, here they are ordered by priority:

- Test the remaining endpoints
- Document with docstring the server module functions
- Create a pipeline for CI/CD

