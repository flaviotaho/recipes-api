cffi==1.15.0
cryptography==36.0.1
pycparser==2.21
pymongo==4.0.1
python-dotenv==0.19.2
toolz==0.11.2
Werkzeug==2.0.2
