import os
from typing import Callable
from toolz import curry
from dotenv import load_dotenv
from src.database.mongodb import connect
from src.lib.server.server import (
        setup_server,
        run_server,
        register,
        create_map_rules,
        application
    )

load_dotenv()

MONGODB_DATABASE_NAME = os.environ.get('MONGODB_DATABASE_NAME')
MONGODB_CONNECTION_URI = os.environ.get('MONGODB_CONNECTION_URI')

rules = list()
handlers = dict()


def register_endpoint(url, method):
    register_rule_and_handler = curry(register)(rules, handlers, url, method)
    db = connect(
        MONGODB_CONNECTION_URI,
        MONGODB_DATABASE_NAME
    )
    def wrapper(handler):
        handler_with_db = curry(handler)(db)
        return register_rule_and_handler(handler_with_db)
    return wrapper



def server_run():
    host = os.environ.get('HOST')
    port = int(os.environ.get('PORT'))
    map_rules = create_map_rules(rules)
    app = application(map_rules, handlers)
    server = setup_server(hostname=host, port=port, application=app)
    print(map_rules)
    run_server(application=app, **server)
