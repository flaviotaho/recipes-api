import pymongo


def connect(connection_string, database_name):
    client = pymongo.MongoClient(connection_string)
    return client.get_database(database_name)


def query_collection(db, collection_name):
    return db[collection_name]
