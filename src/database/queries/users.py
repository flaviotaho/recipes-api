def get(collection, user_id):
    result = collection.find_one({'user_id': user_id})
    return result


def create(collection, new_user):
    result = collection.insert_one(new_user)
    return result
