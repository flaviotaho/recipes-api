from bson import ObjectId


def search(collection, filters, pagination):
    filters = [f for f in filters if f['value'] is not None]
    filters = {f['field']: {f['operator']: f['value']} for f in filters}
    page = pagination['page']
    page_amount = pagination['page_amount']
    recipes_list = [r for r in collection.find(filters).skip(page * page_amount)]
    return recipes_list


def get(collection, recipe_id):
    recipe_id = ObjectId(recipe_id)
    result = collection.find_one({'_id': recipe_id})
    return result


def create(collection, new_recipe):
    result = collection.insert_one(new_recipe)
    return get(collection, result.inserted_id)


def delete(collection, recipe_id):
    recipe_id = ObjectId(recipe_id)
    return collection.delete_one({'_id': recipe_id})


def update(collection, recipe_id, new_recipe):
    return collection.update_one({'_id': ObjectId(recipe_id)}, {'$set': new_recipe})


def rate(collection, recipe_id, rating, user_id):
    new_rating = {
        'recipe_id': ObjectId(recipe_id),
        'rating': rating,
        'user_id': user_id
    }
    result = collection.insert_one(new_rating)
    return get(collection, result.inserted_id)
