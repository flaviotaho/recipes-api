class ServerInitializerException(Exception):
    def __init__(self, server, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.server = server
