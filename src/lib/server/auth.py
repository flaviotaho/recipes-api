import os
import hmac
import hashlib
import base64
import json
from werkzeug.wrappers import Response


AUTH_SECRET_KEY = os.environ.get('AUTH_SECRET_KEY')


def encode_jwt(user_id):
    header = json.dumps({
        'typ': 'JWT',
        'alg': 'HS256'
    }).encode()

    payload = json.dumps({
        'user_id': user_id,
        'exp': 0,
    }).encode()
    b64_header = base64.urlsafe_b64encode(header).decode()
    b64_payload = base64.urlsafe_b64encode(payload).decode()
    signature = hmac.new(
        key=AUTH_SECRET_KEY.encode(),
        msg=f'{b64_header}.{b64_payload}'.encode(),
        digestmod=hashlib.sha256
    ).digest()
    token = f'{b64_header}.{b64_payload}.{base64.urlsafe_b64encode(signature).decode()}'
    return token


def decode_jwt(token):
    b64_header, b64_payload, b64_signature = token.split('.')
    b64_signature_checker = base64.urlsafe_b64encode(
        hmac.new(
            key=AUTH_SECRET_KEY.encode(),
            msg=f'{b64_header}.{b64_payload}'.encode(),
            digestmod=hashlib.sha256
        ).digest()
    ).decode()
    payload = json.loads(base64.urlsafe_b64decode(b64_payload))
    if b64_signature_checker != b64_signature:
        raise Exception('Invalid signature')
    return payload


def authenticate(fn):
    def wrapper(db, request, **kwargs):
        token = request.headers.get('Authorization', '').replace('Bearer ', '')
        if not token:
            error_message = {"error_message": "User not authenticated."}
            response = Response(json.dumps(error_message), content_type='application/json')
            response.status_code = 403
            return response
        authenticated_user = decode_jwt(token)
        request.authenticated_user = authenticated_user
        return fn(db, request, **kwargs)
    return wrapper
