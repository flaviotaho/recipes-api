import uuid
from werkzeug.serving import run_simple
from werkzeug.routing import Rule, Map
from werkzeug.wrappers import Request
from werkzeug.exceptions import HTTPException
from src.lib.server.exceptions import ServerInitializerException


def application(map_rules, handlers):
    def dispatch_request(environ, start_response):
        adapter = map_rules.bind_to_environ(environ)
        try:
            endpoint, values = adapter.match()
            response = handlers[endpoint](Request(environ), **values)
            return response(environ, start_response)
        except HTTPException as e:
            return e(environ, start_response)
    return dispatch_request


def create_map_rules(routes):
    return Map(routes)


def register(rules, handlers, url, method, handler):
    endpoint = str(uuid.uuid4())
    rule = Rule(url, methods=[method], endpoint=endpoint)
    for registered_rule in rules:
        same_url = registered_rule.rule == url
        same_method = method in registered_rule.methods
        if same_url and same_method:
            raise ServerInitializerException('Tried to register duplicated endpoints.')
    rules.append(rule)
    handlers[endpoint] = handler
    return handler


def setup_server(hostname='localhost', port=8080, application=None, **options):
    server_settings = {
            'port': port,
            'hostname': hostname,
            **options
    }
    return server_settings


def run_server(**options):
    run_simple(**options)
