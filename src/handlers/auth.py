import os
import json
from cryptography.fernet import Fernet
from src.app import register_endpoint
from src.database.mongodb import query_collection
from src.database.queries import users
from src.lib.server import auth
from werkzeug.wrappers import Response


FERNET_KEY = os.environ.get('FERNET_KEY')
fernet = Fernet(FERNET_KEY)


@register_endpoint('/signup', 'POST')
def signup(db, request):
    payload = request.json
    user_id = payload['user_id']
    password = payload['password']
    new_user = {
        'user_id': user_id,
        'password': fernet.encrypt(password.encode('utf-8')).decode('utf-8')
    }
    collection = query_collection(db, 'users')
    result = users.create(collection, new_user)
    response = Response()
    if result.inserted_id is not None:
        response.status_code = 204
    else:
        response.status_code = 400
    return response


@register_endpoint('/authenticate', 'POST')
def authenticate(db, request):
    payload = request.json
    user_id = payload['user_id']
    password = payload['password']
    collection = query_collection(db, 'users')
    user = users.get(collection, user_id)
    if not user:
        response = Response()
        response.status_code=403
        return response

    decrypted_db_password = fernet.decrypt(user['password'].encode('utf-8')).decode('utf-8')
    is_authenticated = decrypted_db_password == password
    if not is_authenticated:
        response = Response()
        response.status_code=403
        return response
    result = {
        'authentication': auth.encode_jwt(user_id)
    }
    response = Response(json.dumps(result), mimetype='application/json')
    return response
