import json
from src.app import register_endpoint
from src.database.mongodb import query_collection
from src.database.queries import recipes
from werkzeug.wrappers import Response
from src.lib.server.auth import authenticate


def normalized_param(t, value):
    return t(value) if value is not None else None


@register_endpoint('/recipes', 'GET')
def search(db, request):
    prep_time_min = request.args.get('prep_time_min')
    prep_time_max = request.args.get('prep_time_max')
    difficulty_min = request.args.get('difficulty_min')
    difficulty_max = request.args.get('difficulty_max')
    name = request.args.get("name")
    vegetarian = request.args.get("vegetarian")
    filters = [{
        'field': 'name',
        'operator': 'regex',
        'value': f'/{name}/i' if name else None
    }, {
        'field': 'vegetarian',
        'operator': '$eq',
        'value': normalized_param(bool, vegetarian)
    }, {
        'field': 'prep_time',
        'operator': '$gt',
        'value': normalized_param(int, prep_time_min)
    }, {
        'field': 'prep_time',
        'operator': '$lt',
        'value': normalized_param(int, prep_time_max)
    }, {
        'field': 'difficulty',
        'operator': '$gt',
        'value': normalized_param(int, difficulty_min)
    }, {
        'field': 'difficulty',
        'operator': '$lt',
        'value': normalized_param(int, difficulty_max)
    }]
    pagination = {
        'page': int(request.args.get('page', 0)),
        'page_amount': int(request.args.get('page_amount', 10)),
    }

    collection = query_collection(db, 'recipes')
    recipes_list = recipes.search(collection, filters, pagination)
    result = {
        'recipes': recipes_list,
        'pagination': pagination
    }
    body = json.dumps(result, default=str)
    response = Response(body, mimetype='application/json')
    return response


@register_endpoint('/recipes', 'POST')
@authenticate
def post(db, request):
    collection = query_collection(db, 'recipes')
    new_recipe = request.json
    db_recipe = recipes.create(collection, new_recipe)
    body = json.dumps(db_recipe, default=str)
    response = Response(body, mimetype='application/json')
    return response


@register_endpoint('/recipes/<recipe_id>', 'GET')
def get(db, request, recipe_id):
    collection = query_collection(db, 'recipes')
    db_recipe = recipes.get(collection, recipe_id)
    if not db_recipe:
        response = Response()
        response.status_code = 404
        return response
    body = json.dumps(db_recipe, default=str)
    response = Response(body, mimetype='application/json')
    return response


@register_endpoint('/recipes/<recipe_id>', 'PUT')
@authenticate
def put(db, request, recipe_id):
    new_recipe = request.json
    collection = query_collection(db, 'recipes')
    result = recipes.update(collection, recipe_id, new_recipe)
    response = Response()
    response.status_code = 204
    if result.matched_count == 0:
        response.status_code = 404
    return response


@register_endpoint('/recipes/<recipe_id>', 'DELETE')
@authenticate
def delete(db, request, recipe_id):
    collection = query_collection(db, 'recipes')
    result = recipes.delete(collection, recipe_id)
    response = Response()
    response.status_code = 204
    if result.deleted_count == 0:
        response.status_code = 404
    return response


@register_endpoint('/recipes/<recipe_id>/rating', 'POST')
def rating(db, request, recipe_id):
    payload = request.json
    rating = payload.get('rating')
    user_id = payload.get('user_id')
    if rating is None or user_id is None:
        error_message = {'error_message': 'Rating or user_id invalid.'}
        body = json.dumps(error_message, default=str)
        response = Response(body, mimetype='applcation/json')
        response.status_code = 400
        return response
    collection = query_collection(db, 'recipes_rating')
    db_rating = recipes.rate(collection, recipe_id, rating, user_id)
    body = json.dumps(db_rating, default=str)
    response = Response(body, mimetype='application/json')
    return response
