import os
from src.database.queries import recipes
from werkzeug.test import EnvironBuilder
from werkzeug.wrappers import Request, Response
from src.handlers.recipe import search, post
from unittest.mock import MagicMock


def test_recipe_search(mocker):
    mocker.patch('src.database.queries.recipes.search')
    params = {
        'page': 1,
        'page_amount': 20,
        'name': 'ut',
        'prep_time_max': 30,
        'prep_time_min': 15,
        'difficulty_max': 3,
        'difficulty_min': 1,
        'vegetarian': True
    }
    params = '&'.join([f'{k}={params[k]}' for k in params])
    builder = EnvironBuilder(method='GET', path=f'/recipes?{params}')
    env = builder.get_environ()
    request = Request(env)
    response = search(request)
    expected_filter = [
        {'field': 'name', 'operator': 'regex', 'value': '/ut/i'},
        {'field': 'vegetarian', 'operator': '$eq', 'value': True},
        {'field': 'prep_time', 'operator': '$gt', 'value': 15},
        {'field': 'prep_time', 'operator': '$lt', 'value': 30},
        {'field': 'difficulty', 'operator': '$gt', 'value': 1},
        {'field': 'difficulty', 'operator': '$lt', 'value': 3}
    ]
    pagination = {'page': 1, 'page_amount': 20}
    filter_result = recipes.search.call_args_list[0][0][1]
    assert filter_result == expected_filter
    assert recipes.search.call_args_list[0][0][2] == pagination


def test_recipe_post_authentication(mocker):
    mocker.patch('src.database.queries.recipes.create')
    mocker.patch('src.lib.server.auth.authenticate')
    from src.lib.server.auth import authenticate
    new_recipe = {'vegetarian': True, 'name': 'lasagna'}
    builder = EnvironBuilder(method='POST', path='/recipes', data=new_recipe)
    env = builder.get_environ()
    request = Request(env)
    response = post(request)
    assert response.status_code == 403


def test_recipe_post(mocker):
    mocker.patch('src.database.queries.recipes.create')
    mocker.patch('src.lib.server.auth.authenticate')
    from src.lib.server.auth import authenticate
    new_recipe = '{"vegetarian": true, "name": "lasagna"}'
    builder = EnvironBuilder(method='POST', path='/recipes', json=new_recipe, headers={'Authorization': 'Bearer eyJ0eXAiOiAiSldUIiwgImFsZyI6ICJIUzI1NiJ9.eyJ1c2VyX2lkIjogImZsYXZpbzMiLCAiZXhwIjogMH0=.QMjJ7QmlSiFvBf0015GC8a-gRtcy9aDbHzb2BuMLI7M='})
    env = builder.get_environ()
    request = Request(env)
    response = post(request)
    print(recipes.create.call_args_list)
    assert recipes.create.call_args_list[0][0][1] == new_recipe 
