import pytest
from src.lib.server.server import register

def test_server_endpoint_register():
    rules = list()
    handlers = dict()
    
    def test(request):
        return Response('this is a test')
    register(rules, handlers, '/test', 'GET', test)
    
    assert(rules[0].rule == '/test')
    assert('GET' in rules[0].methods)


def test_server_duplicated_endpoint_registered():
    with pytest.raises(Exception) as e:
        rules = list()
        handlers = dict()
        
        def test(request):
            return Response('this is a test')
        register(rules, handlers, '/test', 'GET', test)
        register(rules, handlers, '/test', 'GET', test)
    assert(e.typename == 'ServerInitializerException')

